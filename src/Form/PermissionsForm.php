<?php

namespace Drupal\trinion_permissions\Form;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\NodeType;
use Drupal\user\Entity\Role;
use Drupal\user\PermissionHandlerInterface;
use Drupal\user\RoleStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the trinion user permissions administration form.
 *
 * @internal
 */
class PermissionsForm extends FormBase {

  /**
   * The permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permissionHandler;

  /**
   * The role storage.
   *
   * @var \Drupal\user\RoleStorageInterface
   */
  protected $roleStorage;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a new UserPermissionsForm.
   *
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   The permission handler.
   * @param \Drupal\user\RoleStorageInterface $role_storage
   *   The role storage.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(PermissionHandlerInterface $permission_handler, RoleStorageInterface $role_storage, ModuleHandlerInterface $module_handler) {
    $this->permissionHandler = $permission_handler;
    $this->roleStorage = $role_storage;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('user.permissions'),
      $container->get('entity_type.manager')->getStorage('user_role'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_permissions_admin_permissions';
  }

  /**
   * Gets the roles to display in this form.
   *
   * @return \Drupal\user\RoleInterface[]
   *   An array of role objects.
   */
  protected function getRoles() {
    return $this->roleStorage->loadMultiple();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $rid = \Drupal::request()->get('rid');
    if (empty($rid)) {
      $form['add_role'] = [
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#title' => 'Add new role',
        '#attributes' => ['class' => 'btn btn-primary mb-3'],
        '#type' => 'link',
        '#url' => Url::fromRoute('user.role_add', [], ['query' => \Drupal::destination()->getAsArray()]),
      ];

      $data = [];
      $theme_handler = \Drupal::service('theme_handler');
      $theme_path = $theme_handler->getTheme('trinion_backend')->getPath();
      $path =  $theme_path . '/templates/svg/edit.svg';
      $svg = file_get_contents($path);

      foreach ($this->getRoles() as $role_name => $role) {
        $data[] = [
          $role->label(),
          \Drupal::linkGenerator()->generate(['#type' => 'inline_template', '#template' => $svg], Url::fromRoute('trinion_permissions.permissions', [], ['query' => ['rid' => $role_name]])),
        ];
      }

      $form['roles'] = [
        '#theme' => 'table',
        '#header' => ['Role', '',],
        '#rows' => $data,
      ];
      return $form;
    }
    else {
      $form['permissions'] = [
        '#type' => 'table',
        '#header' => [$this->t('Permission')],
        '#id' => 'permissions',
      ];
      $ops = ['create', 'edit own', 'edit any', 'delete own', 'delete any',];
      $form['permissions']['#header'] = ['', 'Create', 'Edit own', 'Edit all', 'Delete own', 'Delete all'];

      $permissions = $this->permissionHandler->getPermissions();

      $role_permissions = [];
      foreach ($this->getRoles() as $role_name => $role) {
        $role_permissions[$role_name] = $role->getPermissions();
      }
      foreach ($permissions as $perm_name => $permission) {
        if (preg_match('/^create\s(.*)\scontent$/', $perm_name, $match)) {
          if (NodeType::load($match[1])) {
            $form['permissions'][$match[1]]['description']['#markup'] = t($permission['title']->getArguments()['%type_name']);
            foreach ($ops as $op) {
              $key = "{$op} {$match[1]} content";
              $form['permissions'][$match[1]][$op] = [
                '#type' => 'checkbox',
                '#access' => isset($permissions[$key]),
                '#default_value' => in_array($key, $role_permissions[$rid]) ? 1 : 0,
              ];
              if (!isset($permissions["{$op} {$match[1]} content"]))
                $form['permissions'][$match[1]][$op]['#access'] = FALSE;
            }
          }
        }
      }
    }
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save permissions'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $role_name = \Drupal::request()->get('rid');
    $permissions = $form_state->getValue('permissions');
    $perms = [];
    foreach ($permissions as $bundle => $types) {
      foreach ($types as $type => $val)
        $perms["{$type} {$bundle} content"] = $val;
    }
    user_role_change_permissions($role_name, $perms);

    $this->messenger()->addStatus($this->t('The changes have been saved.'));
  }

  public function getPageTitle() {
    if (($rid = \Drupal::request()->get('rid')) && $role = Role::load($rid)) {
      $title = t('Permissions @role', ['@role' => $role->label()]);
    }
    else {
      $title = t('Permissions');
    }
    return $title;
  }
}
